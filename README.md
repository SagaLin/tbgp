This folder contains the code and data for reproducing results in the paper, Lin et al. (2021+) "Tree-based Gaussian Process with ManyQualitative Factors with Applications toCooling System Design".

---

## Install Packages

Before running the code, users need to install the R package "globpso" which was developed by Ping-Yang Chen and be used for optimization in the paper. The package can be downloaded in [here](https://github.com/PingYangChen/globpso). Other necessary R packages for the code include

1. AdequacyModel(>=2.0.0)
2. RcppArmadillo(>=0.10.5.3.0)
3. plgp(>=1.1.7)
4. foreach(>=1.5.0)
5. doParallel(>=1.0.16)
6. mvtnorm(>=1.1.0)
7. parallel(>=3.6.3)
8. dplyr(>=1.0.6)
9. ggplot2(>=3.3.5)
10. tgp(>=2.4.14)
11. LVGP(>=2.1.5)

which can be directly downloaded and installed from R CRAN.

---

## Description of Each File

This folder contains five R code files: "TBGP.R" "QQGPmodel.R" "simulation_2d.R", "simulation_BoreHole.R", and "real.R", and two data files: "trainData.csv" and "testData.csv". The description of each file is given below.

1. File "TBGP.R" is the main file of the proposed TBGP method.
2. File "QQGPmodel.R" is the man file of QQGP method, which is used for the comparison.
3. File "simulation_2d.R" can reproduce Figures 9, which is running simulated examples of 2-dimensional computer experiments and comparing the proposed method with 100 replicates.
4. File "simulation_BoreHole.R" can reproduce Table 1-2, which is running simulated examples of BoreHole function and comparing the proposed method with 5 replicates. Users may change the settings of levels and change the number of common locations of training data.
5. File "real.R" can reproduce Tabel 4, which analyzes the cooling system experiments based on the proposed TBGP method in the paper.
6. File "trainData.csv" contains the computer experimental data for the cooling system experiments. Three quantitative control variables, "BaseThick", "FinThick" and "FinHeight", all of these variables are in mm, and in the first three columns of the data; three qualitative control variables, "BaseMaterial", "FinMaterial" and "FanType", are in the second three columns of the data, and the corresponding output of each combination of the control variables, "MeanTemp", is in the seventh column. There are 512 samples (i.e., 512 rows), all the locations of quantiitative variables are determined by a 16-run Latin-Hyper Cube sampling, using the common location assumtion with all combinations of qualitative variables.
7. File "testData.csv" contains the computer experimental data for the cooling system experiments. Three quantitative control variables, "BaseThick", "FinThick" and "FinHeight", all of these variables are in mm, and in the first three columns of the data; three qualitative control variables, "BaseMaterial", "FinMaterial" and "FanType", are in the second three columns of the data, and the corresponding output of each combination of the control variables, "MeanTemp", is in the seventh column. There are 512 samples (i.e., 512 rows), all the locations of quantiitative variables are determined by a 16-run Latin-Hyper Cube sampling which is another than trainData.csv, using the common location assumtion with all combinations of qualitative variables.